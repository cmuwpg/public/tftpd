FROM debian:12.5-slim

MAINTAINER Ryan Rempel <rgrempel@cmu.ca>

ENV DEBIAN_FRONTEND=noninteractive \
    BASE_DATE=20220928

RUN mkdir /data

RUN apt-get -qq update && apt-get -qq -y --no-install-recommends install \
   dnsmasq vim

ENTRYPOINT ["/usr/sbin/dnsmasq", "--keep-in-foreground" ] 
